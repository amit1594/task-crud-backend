const connectToMongo = require("./src/helpers/common/init_mongodb");
const { v1 } = require("./src/helpers/common/route_version/v1");
const express = require("express");
require("dotenv").config();
var cors = require("cors");

const app = express();
const port = process.env.PORT || 8000;

connectToMongo();
app.use(cors());
app.use(express.json());

app.use("/v1", v1);

app.listen(port, () => {
  console.log(`iChat listening at http://localhost:${port}`);
});
