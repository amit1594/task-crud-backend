const { Schema, model } = require("mongoose");

const appTaskSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    due_date: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      required: true,
      default: "Pending"
    },
    isDeleted: {
      type: Boolean,
      required: true,
      default: false
    },
  },
  {
    timestamps: true,
  }
);

const appTaskModel = model("app_task", appTaskSchema);

module.exports = {
  appTaskModel,
};
