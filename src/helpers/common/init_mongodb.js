const mongoose = require("mongoose");
const mongoURI =
  "mongodb+srv://16110402:Yadav23%40gm@mycluster.aovlxec.mongodb.net/task?retryWrites=true&w=majority";

const connectToMongo = () => {
  try {
    mongoose.connect(mongoURI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    console.log("Connected to MongoDB");
  } catch (error) {
    console.error("Error connecting to MongoDB:", error.message);
  }
};

module.exports = connectToMongo;
