const { Router } = require("express");
const { appTaskRouterV1 } = require("../../../routes/task/task_routes");

const v1 = Router();
v1.use("/taskroutes", appTaskRouterV1);

module.exports = {
  v1,
};
