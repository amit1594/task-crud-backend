// // Import Packages
const joi = require('joi');

// Define joi Validation Schema
const appAddTaskSchema = joi.object({
  title: joi.string().trim().required(),
  description: joi.string().trim().required(),
  due_date: joi.string().trim().required(),
});

// Export schema
module.exports = {
  appAddTaskSchema
};
