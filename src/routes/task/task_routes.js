const { Router } = require("express");
const {
  addTask,
  getTask,
  filterTask,
  updateTask
} = require("../../controllers/task/task_controller");

const appTaskRouterV1 = Router();

appTaskRouterV1.post(
  "/add-task",
  addTask
);

appTaskRouterV1.get(
  "/get-task",
  getTask
);

appTaskRouterV1.post(
  "/filter-task",
  filterTask
);

appTaskRouterV1.post(
  "/update-task",
  updateTask
);

module.exports = {
  appTaskRouterV1,
};
