const {
  appAddTaskSchema,
} = require("../../helpers/joi/task_joi");
const { appTaskModel } = require("../../models/task/Task.js");
const httpErrors = require("http-errors");
require("dotenv").config();

const addTask = async (req, res) => {
  try {
    //validate joi schema
    const appTaskDetails = await appAddTaskSchema.validateAsync(req.body);

    console.log(appTaskDetails, "appTaskDetails");
    const task_detail = new appTaskModel({
      title: appTaskDetails.title,
      description: appTaskDetails.description,
      due_date: appTaskDetails.due_date,
    })

    await task_detail.save({});
      
    // Send Response
    res.status(200).send({
      error: false,
      data: {
        data: appTaskDetails,
        message: "Task create successfully.",
      },
    });
  } catch (error) {
    res.status(200).send({
      error: true,
      reason: error,
    });
  }
};
const getTask = async (req, res) => {
  try {
    //validate joi schema
    const all_task_detail = await appTaskModel.find({});
      
    // Send Response
    res.status(200).send({
      error: false,
      data: {
        data: all_task_detail,
        message: "Task create successfully.",
      },
    });
  } catch (error) {
    res.status(200).send({
      error: true,
      reason: error,
    });
  }
};
const filterTask = async (req, res) => {
  try {
    //validate joi schema
    const all_task_detail = await appTaskModel.find({status: req.body.status});
      
    // Send Response
    res.status(200).send({
      error: false,
      data: {
        data: all_task_detail,
        message: "Task create successfully.",
      },
    });
  } catch (error) {
    res.status(200).send({
      error: true,
      reason: error,
    });
  }
};
const updateTask = async (req, res) => {
  try {
    //validate joi schema
    console.log(req.body, "all_task_detail");
    const all_task_detail = await appTaskModel.updateOne(
      {
        _id: req.body.id
      },
      {
        $set: 
        {
          status: req.body.status
        }
      });
      
    // Send Response
    res.status(200).send({
      error: false,
      data: {
        data: all_task_detail,
        message: "Task update successfully.",
      },
    });
  } catch (error) {
    res.status(200).send({
      error: true,
      reason: error,
    });
  }
};

module.exports = {
  addTask,
  getTask,
  filterTask,
  updateTask
};
